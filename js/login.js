
    // Import the functions you need from the SDKs you need
    import { getAuth, signInWithEmailAndPassword } from "https://www.gstatic.com/firebasejs/10.5.2/firebase-auth.js";

    import { initializeApp } from "https://www.gstatic.com/firebasejs/10.5.2/firebase-app.js";
    // TODO: Add SDKs for Firebase products that you want to use
    // https://firebase.google.com/docs/web/setup#available-libraries
  
    // Your web app's Firebase configuration
    const firebaseConfig = {
      apiKey: "AIzaSyDOyUe-E8wLtBaToSnkbF55OOrOIwLgFkk",
      authDomain: "proyectofinal-247a6.firebaseapp.com",
      projectId: "proyectofinal-247a6",
      storageBucket: "proyectofinal-247a6.appspot.com",
      messagingSenderId: "446233740542",
      appId: "1:446233740542:web:764564326c46c58e7911af"
    };
  
    // Initialize Firebase
    const app = initializeApp(firebaseConfig);
    const auth = getAuth(app);

const formulario = document.getElementById("formulario");
const emailInput = document.getElementById("exampleInputEmail1");
const passwordInput = document.getElementById("exampleInputPassword1");
const errorMensaje = document.getElementById("errorMensaje");


formulario.addEventListener("submit", async (e) => {
  e.preventDefault(); 

  const email = emailInput.value;
  const password = passwordInput.value;

  try {
 
    await signInWithEmailAndPassword(auth, email, password);
    
    window.location.href = "agregar producto.html";
    console.log("se ingreso correctamente");
  } catch (error) {
    if(email == "" || password == "")
    errorMensaje.textContent = "Ingresa el campo vacio";
  else{
    errorMensaje.textContent = "Error de inicio de sesión. Verifica tu correo y contraseña.";
    alert("Error de inicio de sesión. Verifica tu correo y contraseña.")
    console.error("Error de inicio de sesión:", error);
  }
  }
});




