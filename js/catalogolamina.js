// Importa las funciones que necesitas desde los SDK que necesitas
import { initializeApp } from "https://www.gstatic.com/firebasejs/10.5.2/firebase-app.js";
import { getStorage, ref as storageRef, getDownloadURL } from "https://www.gstatic.com/firebasejs/10.5.2/firebase-storage.js";
import { getDatabase, ref as dbRef, get } from "https://www.gstatic.com/firebasejs/10.5.2/firebase-database.js";

// Configuración de Firebase
const firebaseConfig = {
    apiKey: "AIzaSyCR3XG2xk9T3RatyWGvvyDh_CUTzHFz7Wc",
    authDomain: "proyecto-final-f3f12.firebaseapp.com",
    databaseURL: "https://proyecto-final-f3f12-default-rtdb.firebaseio.com",
    projectId: "proyecto-final-f3f12",
    storageBucket: "proyecto-final-f3f12.appspot.com",
    messagingSenderId: "52181647355",
    appId: "1:52181647355:web:0ba685f2443ed24b54555e"
};

// Inicializa Firebase
const app = initializeApp(firebaseConfig);
const storage = getStorage(app);
const db = getDatabase(app);

const productosContainer = document.getElementById('productosContainer');

const mostrarProductosEnTienda = (productos) => {
    productosContainer.innerHTML = ''; // Limpiar el contenedor

    productos.forEach(async (producto) => {
        const itemDiv = document.createElement('div');
        itemDiv.className = 'item';

        const figure = document.createElement('figure');
        const img = document.createElement('img');

        // Obtén la URL de la imagen desde Storage
        const imageUrl = await getDownloadURL(storageRef(storage, producto.imagen));
        img.src = imageUrl || '';
        img.alt = producto.nombre || '';
        figure.appendChild(img);

        const infoProductDiv = document.createElement('div');
        infoProductDiv.className = 'info-product';
        const h2 = document.createElement('h2');
        h2.textContent = producto.nombre || '';
        const p = document.createElement('p');
        p.className = 'price';
        p.textContent = `$${producto.precio || ''}`;
   

        infoProductDiv.appendChild(h2);
        infoProductDiv.appendChild(p);

        const btnRemove = document.createElement('button');
        btnRemove.textContent = '-';
        btnRemove.className = 'btn-remove-cart';
        

        const btnAdd = document.createElement('button');
        btnAdd.textContent = '+';
        btnAdd.className = 'btn-add-cart';

        infoProductDiv.appendChild(btnRemove);
        infoProductDiv.appendChild(btnAdd);


        itemDiv.appendChild(figure);
        itemDiv.appendChild(infoProductDiv);

        productosContainer.appendChild(itemDiv);
    });
};

const actualizarTabla = async () => {
    try {
        const productosRef = dbRef(db, 'productos');
        const snapshot = await get(productosRef);
        const productos = [];

        snapshot.forEach((childSnapshot) => {
            productos.push(childSnapshot.val());
        });

        mostrarProductosEnTienda(productos);
        console.log('Productos obtenidos con éxito:', productos);
    } catch (error) {
        console.error('Error al obtener productos:', error);
    }
};

document.addEventListener('DOMContentLoaded', () => {
    console.log('La página ha cargado');
    actualizarTabla();
});

function irALogin() {
    // Reemplazar la página actual con login.html
    window.location.replace('login.html');
  }