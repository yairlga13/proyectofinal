  // Import the functions you need from the SDKs you need
  import { initializeApp } from "https://www.gstatic.com/firebasejs/10.5.2/firebase-app.js";
  import { getStorage, ref, uploadBytes, getDownloadURL, deleteObject} from "https://www.gstatic.com/firebasejs/10.5.2/firebase-storage.js";
  import { getDatabase, ref as dbRef, set , remove, get, } from "https://www.gstatic.com/firebasejs/10.5.2/firebase-database.js";
 
   // TODO: Add SDKs for Firebase products that you want to use
   // https://firebase.google.com/docs/web/setup#available-libraries
 
   // Your web app's Firebase configuration
   const firebaseConfig = {
     apiKey: "AIzaSyCR3XG2xk9T3RatyWGvvyDh_CUTzHFz7Wc",
     authDomain: "proyecto-final-f3f12.firebaseapp.com",
     databaseURL: "https://proyecto-final-f3f12-default-rtdb.firebaseio.com",
     projectId: "proyecto-final-f3f12",
     storageBucket: "proyecto-final-f3f12.appspot.com",
     messagingSenderId: "52181647355",
     appId: "1:52181647355:web:0ba685f2443ed24b54555e"
   };
 
 
 // Initialize Firebase
 const app = initializeApp(firebaseConfig);
 
 const storage = getStorage(app);
 const db = getDatabase(app);
 
 const formularioProductos = document.getElementById("formularioProductos");
 const btnagregar = document.getElementById("btnagregar");
 
 btnagregar.addEventListener("click", async () => {
 
   const codigo = document.getElementById("txtCodigo").value;
   const nombre = document.getElementById("txtNombre").value;
   const precio = document.getElementById("txtPrecio").value;
   const imageInput = document.getElementById("imageInput");
 
   if (!codigo || !nombre || !precio || !imageInput.files[0]) {
     alert("Por favor, complete todos los campos y seleccione una imagen.");
     return;
   }
 
   const productoSnapshot = await get(dbRef(db, `productos/${codigo}`));
   if (productoSnapshot.exists()) {
     alert("El código ingresado ya existe. Por favor, ingrese un código único.");
     return; 
   }
 
   const imageRef = ref(storage, `images/${codigo}`);
   await uploadBytes(imageRef, imageInput.files[0]);
 
   const imageUrl = await getDownloadURL(imageRef);
 
   const productoRef = dbRef(db, `productos/${codigo}`);
   set(productoRef, {
     codigo,
     nombre,
     precio,
     imagen: imageUrl,
   });
   console.log("Producto agregado con éxito");
 
   formularioProductos.reset();
 });
 
 const btnBorrar = document.getElementById("btnBorrar");
 
 btnBorrar.addEventListener("click", async () => {
   const codigo = document.getElementById("txtCodigo").value;
 
   const productoRef = dbRef(db, `productos/${codigo}`);
 
  
   const productoSnapshot = await get(productoRef);
   const producto = productoSnapshot.val();
 
   
   if (productoSnapshot.exists()) {
     
     const imageRef = ref(storage, `images/${codigo}`);
     await deleteObject(imageRef);
 
    
     await remove(productoRef);
 
 
     formularioProductos.reset();
   } else {
    
     alert("Producto no encontrado");
   }
 });
 
 
 
 const btnActualizar = document.getElementById("btnActualizar");
 
 btnActualizar.addEventListener("click", async () => {
   const codigo = document.getElementById("txtCodigo").value;
   const nombre = document.getElementById("txtNombre").value;
   const precio = document.getElementById("txtPrecio").value;
   const imageInput = document.getElementById("imageInput");
 
   
   if (!codigo || !nombre || !precio || !imageInput.files[0]) {
     alert("Por favor, complete todos los campos y seleccione una imagen.");
     return; 
   }
 
   const productoSnapshot = await get(dbRef(db, `productos/${codigo}`));
 
   if (productoSnapshot.exists()) {
     const oldImageRef = ref(storage, `images/${codigo}`);
     await deleteObject(oldImageRef);
 
     const newImageRef = ref(storage, `images/${codigo}`);
     await uploadBytes(newImageRef, imageInput.files[0]);
 
     const imageUrl = await getDownloadURL(newImageRef);
 
     const productoRef = dbRef(db, `productos/${codigo}`);
     set(productoRef, {
       codigo,
       nombre,
       precio,
       imagen: imageUrl,
     });
 
     console.log("Producto actualizado con éxito");
 
     formularioProductos.reset();
   } else {
     alert("El código ingresado no existe. No se puede actualizar.");
   }
 });
 
 const tablaMuestra = document.getElementById("tablamuestra").getElementsByTagName('tbody')[0];
 
 const actualizarTabla = async () => {
 
   const productosRef = dbRef(db, 'productos');
 
   const snapshot = await get(productosRef);
 
   tablaMuestra.innerHTML = '';
 
   snapshot.forEach((childSnapshot) => {
     const producto = childSnapshot.val();
     const fila = tablaMuestra.insertRow();
 
 
     const codigoCell = fila.insertCell(0);
     codigoCell.textContent = producto.codigo;
 
     const nombreCell = fila.insertCell(1);
     nombreCell.textContent = producto.nombre;
 
     const precioCell = fila.insertCell(2);
     precioCell.textContent = "$"+producto.precio;
 
     const imagenCell = fila.insertCell(3);
     const imagen = document.createElement('img');
     imagen.src = producto.imagen;
     imagen.alt = 'Imagen del producto';
     imagenCell.appendChild(imagen);
   });
 };
 
 actualizarTabla();
 
 const btnBuscar = document.getElementById('btnBuscar');
 btnBuscar.addEventListener('click', async function () {
     event.preventDefault();
 
     const codigoABuscar = document.getElementById('txtCodigo').value;
 
     if (codigoABuscar) {
         try {
 
             const productoRef =dbRef(db, `productos/${codigoABuscar}`)
             const snapshot = await get(productoRef);
 
             if (snapshot.exists()) {
                 const producto = snapshot.val();
                 const nombre = producto.nombre;
                 const descripcion = producto.descripcion;
                 const precio = producto.precio;
                 const imageUrl = producto.imagen; 
 
 
                 tablamuestra.innerHTML = '';
 
                 const newRow = tablamuestra.insertRow(tablamuestra.rows.length);
                 const codigoCell = newRow.insertCell(0);
                 const nombreCell = newRow.insertCell(1);
                 const descripcionCell = newRow.insertCell(2);
                 const precioCell = newRow.insertCell(3);
                 const imagenCell = newRow.insertCell(4);
 
                 codigoCell.textContent = codigoABuscar;
                 nombreCell.textContent = nombre;
                 descripcionCell.textContent = descripcion;
                 precioCell.textContent = precio;
 
                 const img = document.createElement("img");
                 img.src = imageUrl;
                 img.width = 100;
                 imagenCell.appendChild(img);
             } else {
                 alert('Producto no encontrado.');
             }
         } catch (error) {
             console.error('Error al buscar el producto:', error);
         }
     } else {
         alert('Ingresa un código de producto válido para buscar.');
     }
 });
 
 
 const reload = document.getElementById('reload');
 reload.addEventListener('click',function(){
     location.reload();
 });
 
 const btnLimpiarFormulario = document.getElementById('btnLimpiarFormulario');
 btnLimpiarFormulario.addEventListener('click', function () {
     const formulario = document.getElementById('formularioProductos');
 
     formulario.reset();
 });
 