// Función que se ejecuta cuando se carga la página
document.addEventListener('DOMContentLoaded', function () {
    const btnCart = document.querySelector('.container-cart-icon');
    const containerCartProducts = document.querySelector('.container-cart-products');
    const addToCartButtons = document.querySelectorAll('.btn-add-cart');
    const removeFromCartButtons = document.querySelectorAll('.btn-remove-cart');
    const valorTotal = document.querySelector('.total-pagar');
    const countProducts = document.querySelector('#contador-productos');
    const cartEmpty = document.querySelector('.cart-empty');
    const cartTotal = document.querySelector('.cart-total');
    const rowProduct = document.querySelector('.row-product');
    const productsList = document.querySelector('.container-items');
    let allProducts = [];
    // Función para mostrar HTML
    const showHTML = () => {
      if (!allProducts.length) {
        cartEmpty.classList.remove('hidden');
        rowProduct.classList.add('hidden');
        cartTotal.classList.add('hidden');
      } else {
        cartEmpty.classList.add('hidden');
        rowProduct.classList.remove('hidden');
        cartTotal.classList.remove('hidden');
      }
  
      // Limpiar HTML
      rowProduct.innerHTML = '';
      let total = 0;
      let totalOfProducts = 0;
  
      allProducts.forEach(product => {
        const containerProduct = document.createElement('div');
        containerProduct.classList.add('cart-product');
  
        const cantidadProduct = document.createElement('span');
        cantidadProduct.classList.add('cantidad-producto-carrito');
        cantidadProduct.textContent = product.quantity;
  
        const tituloProduct = document.createElement('p');
        tituloProduct.classList.add('titulo-producto-carrito');
        tituloProduct.textContent = product.title;
  
        const precioProduct = document.createElement('span');
        precioProduct.classList.add('precio-producto-carrito');
        const priceValue = parseFloat(product.price.slice(1)) * product.quantity;
        precioProduct.textContent = `$${priceValue.toFixed(2)}`;
  
        containerProduct.appendChild(cantidadProduct);
        containerProduct.appendChild(tituloProduct);
        containerProduct.appendChild(precioProduct);
  
        rowProduct.appendChild(containerProduct);
  
        total += priceValue;
        totalOfProducts += product.quantity;
      });
  
      valorTotal.innerText = `$${total.toFixed(2)}`;
      countProducts.innerText = totalOfProducts;
    };
  
    btnCart.addEventListener('click', () => {
      containerCartProducts.classList.toggle('hidden-cart');
    });
  
    // Evento click en los botones de agregar al carrito
    productsList.addEventListener('click', e => {
      if (e.target.classList.contains('btn-add-cart')) {
        const product = e.target.parentElement;
  
        const infoProduct = {
          quantity: 1,
          title: product.querySelector('h2').textContent,
          price: product.querySelector('p').textContent,
        };
  
        const exists = allProducts.some(
          product => product.title === infoProduct.title
        );
  
        if (exists) {
          const products = allProducts.map(product => {
            if (product.title === infoProduct.title) {
              product.quantity++;
              return product;
            } else {
              return product;
            }
          });
          allProducts = [...products];
        } else {
          allProducts = [...allProducts, infoProduct];
        }
  
        showHTML();
      }
  
      if (e.target.classList.contains('btn-remove-cart')) {
        const product = e.target.parentElement;
        const title = product.querySelector('h2').textContent;
  
        const productIndex = allProducts.findIndex(product => product.title === title);
  
        if (productIndex !== -1) {
          if (allProducts[productIndex].quantity > 1) {
            allProducts[productIndex].quantity--;
          } else {
            allProducts.splice(productIndex, 1);
          }
  
          showHTML();
        }
      }
    });
  
    // Evento click para eliminar un producto del carrito
    rowProduct.addEventListener('click', e => {
      if (e.target.classList.contains('icon-close')) {
        const product = e.target.parentElement;
        const title = product.querySelector('p').textContent;
  
        allProducts = allProducts.filter(product => product.title !== title);
        showHTML();
      }
    });
  
    // Función para manejar la selección en el menú
    const menuItems = document.querySelectorAll('.menu li');
  
    menuItems.forEach(menuItem => {
      menuItem.addEventListener('click', () => {
        const selectedCategory = menuItem.getAttribute('data-page');
  
        const allProducts = document.querySelectorAll('.item');
        allProducts.forEach(product => {
          product.style.display = 'none';
        });
        if (selectedCategory === 'Todos') {
          // Si se selecciona "Todos", muestra todos los productos excepto la categoría "Inicio"
          allProducts.forEach(product => {
              if (product.getAttribute('data-category') !== 'Inicio') {
                  product.style.display = 'block';
              }
          });

          } else {
            // Muestra solo los productos de la categoría seleccionada
            const selectedProducts = document.querySelectorAll(`.item[data-category="${selectedCategory}"]`);
            selectedProducts.forEach(product => {
              product.style.display = 'block';
        });
      }
    });
  });
});
 // Espera a que se cargue el contenido de la página
  window.addEventListener("load", function () {
    // Mostrar la sección de "Inicio"
    document.getElementById("seccion-inicio").style.display = "block";
    // Ocultar otras secciones
    var secciones = document.querySelectorAll(".item:not(#seccion-inicio)");
    secciones.forEach(function (seccion) {
      seccion.style.display = "none";
    });
  });

//carrusel

document.addEventListener('DOMContentLoaded', function () {
  const imagenActual = document.getElementById('imagen-en-movimiento');
  const imagenes = ['/img/Carrucel/img2.webp', '/img/Carrucel/img3.avif', '/img/Carrucel/img4.webp']; // Agrega las rutas de tus imágenes
  let currentIndex = 0;

  function cambiarImagen() {
      currentIndex = (currentIndex + 1) % imagenes.length;
      const nuevaImagen = imagenes[currentIndex];

      imagenActual.style.animation = 'desplazar-izquierda 1s forwards';

      setTimeout(function () {
          // Actualiza la imagen y reinicia la animación
          imagenActual.src = nuevaImagen;
          imagenActual.style.animation = '';
      }, 500); // Espera 1 segundo para la transición
  }

  // Inicia el intervalo para cambiar la imagen cada 5 segundos
  setInterval(cambiarImagen, 5000); // 5000 milisegundos (5 segundos)
});
 // Espera a que se cargue el contenido de la página
 window.addEventListener("load", function () {
  // Mostrar la sección de "Inicio"
  document.getElementById("seccion-inicio").style.display = "block";
  // Ocultar otras secciones
  var secciones = document.querySelectorAll(".item:not(#seccion-inicio)");
  secciones.forEach(function (seccion) {
    seccion.style.display = "none";
  });
});
function irALogin() {
  // Reemplazar la página actual con login.html
  window.location.replace('/html/login.html');
}